/*���� III. �������� 
��� ������ �� ��������� ������ �������� ������� ���������������� ��������: users_log_trigger, roles_log_trigger, users_to_roles_log_trigger. 
������ �� ��������� ������� ���������� � ������� MYLOGS 
��������, ������� roles_log_trigger ������� ������ ����:  
ACTION=>U,  
TBL_NAME=>ROLES,  
ID_ROW=>12,  
LOG_INFO=> ����: ID=12,NAME=TEST_ROLE,RULE=UID �����: ID=12,NAME=TEST_ROLE,RULE=U, 
LOG_TIME=>getdate()  */
use AdventureWorks2012;
GO
	CREATE TRIGGER users_log_trigger
	ON users FOR UPDATE
	AS 
	BEGIN
		DECLARE
			@log_info_ID_NEW INT=(select id from inserted),
			@log_info_ID_OLD INT=(select id from deleted),
			@log_info_USER_NAME_NEW NVARCHAR(100)=(select [USER_NAME] from inserted),
			@log_info_USER_NAME_OLD NVARCHAR(100)=(select [USER_NAME] from deleted),
			@LOG_TIME DATETIME=getdate(),
			@action NCHAR(100)=N'U',
			@table_name NVARCHAR(100)=N'USERS',
			@str varchar(255)
			set @str=N'old:ID='+cast(@log_info_ID_OLD as nvarchar(10))+N' ,NAME='+@log_info_USER_NAME_OLD
					+N' new:ID= '+cast(@log_info_ID_NEW as nvarchar(10))+N' ,NAME='+@log_info_USER_NAME_NEW;

		INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
		@action,@table_name,@log_info_ID_NEW,@str,@LOG_TIME)
	END
GO
	CREATE TRIGGER roles_log_trigger
	ON roles FOR UPDATE
	AS 
	BEGIN
		DECLARE
			@role_id_new INT=(SELECT ID FROM inserted),
			@role_id_old INT=(SELECT ID FROM deleted),
			@rule_new nchar(1)=(SELECT [RULE] FROM inserted),
			@rule_old nchar(1)=(SELECT [RULE] FROM deleted),
			@role_name_new NVARCHAR(100)=(SELECT [ROLE_NAME] FROM inserted),
			@role_name_old NVARCHAR(100)=(SELECT [ROLE_NAME] FROM deleted),
			@log_time DATETIME=getdate(),
			@action NCHAR(100)=N'U',
			@table_name NVARCHAR(100)=N'ROLES',
			@str varchar(255)
		SET @str=N'old:ID='+cast(@role_id_old as nvarchar(10))+N' NAME:'+@role_name_old+N' RULE:'+cast(@rule_old as nvarchar(1))
				+N' new:ID='+cast(@role_id_new as nvarchar(10))+N' NAME:'+@role_name_new+N' RULE:'+cast(@rule_new as nvarchar(1))

		INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
			@action,@table_name,@role_id_new,@str,@LOG_TIME)
	END
GO
	CREATE TRIGGER users_to_roles_log_trigger
	ON USERS_TO_ROLES FOR UPDATE
	AS 
	BEGIN
		DECLARE @id_new INT=(SELECT ID FROM inserted),
			@id_old INT=(SELECT ID FROM inserted),
			@role_id_new INT=(SELECT ROLE_ID FROM inserted),
			@role_id_old INT=(SELECT ROLE_ID FROM deleted),
			@user_id_new INT=(SELECT [USER_ID] FROM inserted),
			@user_id_old INT=(SELECT [USER_ID] FROM deleted),
			@LOG_TIME DATETIME=getdate(),
			@action NCHAR(100)=N'U',
			@table_name NVARCHAR(100)=N'USERS_TO_ROLES',
			@str varchar(255)
		SET @str=N'old:ID='+cast(@id_old as nvarchar(10))+N' ROLE_ID='+cast(@role_id_old as nvarchar(10))+N' USER_ID'+cast(@user_id_old as nvarchar(10))
				+N' new:ID'+cast(@id_new as nvarchar(10))+N' ROLE_ID='+cast(@role_id_new as nvarchar(10))+N' USER_ID'+cast(@user_id_new as nvarchar(10))
		
		INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
		   @action,@table_name,@id_new,@str,@LOG_TIME)
	END
GO

	update USERS set [USER_NAME]=N'result' Where ID=3;
	update USERS_TO_ROLES set [ROLE_iD]=5 Where [iD]=3;
	update ROLES set [RULE]=N'R' Where [ID]=4;

	select * from MYLOGS