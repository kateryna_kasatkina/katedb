/*1. �������� �������, ��������� ������� ��������� ���� � �����������
 ��� ��������������� ���� ���� ������ (AdventureWorks ��� Northwind). */
use AdventureWorks2012;
CREATE TABLE [dbo].[USERS]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [USER_NAME] [NVARCHAR] (100)
)

CREATE TABLE [dbo].[ROLES]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [ROLE_NAME] [NVARCHAR](100),
  [RULE] [NCHAR]
)

CREATE TABLE [dbo].[USERS_TO_ROLES]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [USER_ID] [INT],
  [ROLE_ID] [INT],
  FOREIGN KEY (USER_ID) REFERENCES USERS(ID),
  FOREIGN KEY (ROLE_ID) REFERENCES ROLES(ID)
)

CREATE TABLE [dbo].[MYLOGS]
(
  [MYLOGS_ID] int IDENTITY(1,1) PRIMARY KEY ,
  [ACTIONN] [NCHAR](100),
  [TBL_NAME] [NVARCHAR](100),
  [ID_ROW] [INT],
  [LOG_INFO] [NVARCHAR](100),
  [LOG_TIME] [DATETIME]
)