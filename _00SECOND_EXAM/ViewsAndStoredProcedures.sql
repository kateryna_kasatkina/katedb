/*1. �������� ��� View ��� ������� MYLOGS: ������ ROLE_LOG_VIEW, 
������� ���������� ��� ��������� � ������� ROLE, ������ � USER_LOG_VIEW, 
������� ���������� ����� ��������� ����������� � ������� USER. */

use AdventureWorks2012;

go
create view ROLE_LOG_VIEW as 
select
MYLOGS.TBL_NAME,
MYLOGS.LOG_INFO,
MYLOGS.LOG_TIME
from MYLOGS
where MYLOGS.Tbl_Name=N'ROLES'

go
create view USER_LOG_VIEW as 
select
MYLOGS.TBL_NAME,
MYLOGS.LOG_INFO,
MYLOGS.LOG_TIME
from MYLOGS
where MYLOGS.Tbl_Name=N'USERS'

/*2. �������� �������� ������� udf_count_user_rule,
������� ��������� ���������� �������������, � ������� ���� �������� �����.
(������ U � ������, ������� �������� � ������� RULE � �������� ����� ��������� ������, 
������ I � ����� ��������� ������, ������ D � ����� �������) 
������ ������ �������, ��� ����������� ���������� ������������� � ������� U (Update): 
select dbo.udf_count_user_rule(�U�) as C_Us_U; */

use AdventureWorks2012;
go

IF OBJECT_ID(N'dbo.udf_count_user_rule') IS NOT NULL
    DROP FUNCTION dbo.udf_count_user_rule;
GO

create function udf_count_user_rule (@r nchar(1))
returns int
as
begin
	declare @c int
	set @c=(select count([RULE])from ROLES where [RULE]=@r)
	return (@c)
end

--select dbo.udf_count_user_rule('D') as C_Us_U;

/*3. �������� �������� ������� udf_show_user_rule ������� ����������
 ������ ���� ��������� ������������, � ��������� �������� ID ������������. ������ ������ : 
select dbo.udf_show_user_rule (12) as USERS_RULE;*/

IF OBJECT_ID(N'dbo.udf_show_user_rule') IS NOT NULL
    DROP FUNCTION dbo.udf_show_user_rule;
GO

create function udf_show_user_rule (@id int)
RETURNS @retUserRules TABLE 
(
    RoleName nvarchar(100) NULL
)
as
begin
	insert @retUserRules
	select distinct ROLES.ROLE_NAME
		from USERS_TO_ROLES
		join ROLES on USERS_TO_ROLES.ROLE_ID=ROLES.ID
		join USERS on USERS_TO_ROLES.[USER_ID]=[USERS].ID
		WHERE USERS_TO_ROLES.[USER_ID]=@id;
	return
end
GO

--SELECT * FROM udf_show_user_rule (1) as USERS_RULE

/*4. �������� �������� ������� udf_show_user_for_rule 
������� ���������� ������ �������������, � ������� ���� �������� �����. ������ ������ : 
select dbo.udf_show_user_for_rule (�U�) as User_for_U; */

IF OBJECT_ID(N'dbo.udf_show_user_for_rule') IS NOT NULL
    DROP FUNCTION dbo.udf_show_user_for_rule;
GO

create function udf_show_user_for_rule(@rule nchar(1))
returns table
as
return(
	select distinct [USERS].[USER_NAME]
	from [USERS_TO_ROLES]
	join [USERS] on [USERS_TO_ROLES].[USER_ID]=[USERS].ID
	join [ROLES] on [USERS_TO_ROLES].[ROLE_ID]=[ROLES].ID
	where [ROLES].[RULE]=@rule
)

select * from udf_show_user_for_rule (N'D') as User_for_U;