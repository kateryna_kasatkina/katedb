/*�������� ��������� 1. ��� ������ �� ��������� ������ � ����� I �������� �������� ���������,
-- ������� ��������� ��� ���������� ���� ������� � �������� ������� ���������� ����������� 
--�������� 
-- id ������� ������ ��� ����������� ������ � ���� ��������� ���������. 
-- (����� 4 ���������: ADD_ROLE, ADD_USER, ADD_USER_TO_ROLE, ADD_LOG)  */
/*Declare @res int set @res = -1;
 Execute [dbo].ADD_ROLE �TEST_ROLE� ,�IUD�,
  @res output select @res as id_role*/

use AdventureWorks2012;

go
create proc ADD_ROLE(@name nvarchar(100),@role nchar(1),@id int output)
as
begin
	insert into  ROLES values(@name,@role)
	set @id=SCOPE_IDENTITY()
end

--drop proc ADD_ROLE
select * from ROLES

Declare @res int set @res = -1;
Execute ADD_ROLE 'TEST_ROLE','IU',@res output 
select @res as id_role


go
create proc ADD_USER(@name nvarchar(100),@id int output)
as
begin
	insert into users values (@name)
	set @id=SCOPE_IDENTITY()
end

--drop proc ADD_USER
select * from USERS


go
create proc ADD_USER_TO_ROLE(@id_user int,@id_role int,@id int output)
as
begin 
	insert into USERS_TO_ROLES values (@id_user,@id_role)
	--SELECT SCOPE_IDENTITY()
	set @id=SCOPE_IDENTITY()
end

--drop proc ADD_USER_TO_ROLE
select * from USERS_TO_ROLES

go
create proc ADD_LOG (
@action nchar(100),
@table_name nvarchar(100),
@id_row int,
@log_info nvarchar(100),@log_time datetime,@id int output)
as
begin
	insert into MYLOGS values (@action,@table_name,@id_row,@log_info,@log_time)
	--SELECT SCOPE_IDENTITY()
	set @id=SCOPE_IDENTITY()
end

--drop proc ADD_LOG
select * from MYLOGS
