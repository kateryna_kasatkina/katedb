/*3. ������ ������� ������ ��������� ������ � ������� ����� MYLOGS. 
��������, ������� my_safety_create_log_trigger ��������� ������� ���:
 ACTION=>CREATE_TABLE, TBL_NAME=>NOTDEF, ID_ROW=>-1, LOG_INFO=> ��������� �������� CREATE_TABLE, LOG_TIME=>getdate() 
������� � �������� ��������� � �������� � ������ Task13 */
 /*3. ������ ������� ������ ��������� ������ � ������� ����� MYLOGS. 
  ��������, ������� my_safety_create_log_trigger ��������� ������� ���: ACTION=>CREATE_TABLE,
  TBL_NAME=>NOTDEF, ID_ROW=>-1, LOG_INFO=> ��������� �������� CREATE_TABLE, LOG_TIME=>getdate() */

GO
   CREATE TRIGGER my_safety_create_log_trigger1
   ON DATABASE 
   FOR CREATE_TABLE
   AS 
   BEGIN
	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Create table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� CREATE_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
   END

   GO
   CREATE TRIGGER my_safety_drop_log_trigger1
   ON DATABASE 
   FOR DROP_TABLE
   AS 
   BEGIN
	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Drop table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� DROP_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
   END


   GO
   CREATE TRIGGER my_safety_alter_log_trigger1
   ON DATABASE 
   FOR ALTER_TABLE
   AS 
   BEGIN
	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Alter table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� ALTER_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
   END