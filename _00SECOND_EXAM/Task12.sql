/*2. �������� �������� ������ ���� ������ my_safety_create_log_trigger, 
������� ��������� �� ������� CREATE_TABLE, my_safety_drop_log_trigger (DROP TABLE), 
my_safety_alter_log_trigger (ALTER TABLE). 
������ ������� ������ �������� ��������� � �������. 
��������, ������� my_safety_create_log_trigger  ������ �������� ��������� ���������� �������� CREATE_TABLE�. 
������� � �������� ��������� � �������� � ������ Task12 */

use AdventureWorks2012;
   GO
   CREATE TRIGGER my_safety_create_log_trigger
   ON DATABASE 
   FOR CREATE_TABLE
   AS 
   PRINT N'��������� �������� CREATE_TABLE' 
  
   GO
   CREATE TRIGGER my_safety_drop_log_trigger
   ON DATABASE 
   FOR DROP_TABLE
   AS 
   PRINT N'��������� �������� DROP_TABLE' 
  
   GO
   CREATE TRIGGER my_safety_alter_log_trigger
   ON DATABASE 
   FOR ALTER_TABLE
   AS 
   PRINT N'��������� �������� ALTER_TABLE' 

