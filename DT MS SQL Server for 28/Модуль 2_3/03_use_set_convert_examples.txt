--������ �������������� �����
declare @A varchar(2)
declare @B varchar(2)
declare @C varchar(2)
declare @D int
set @A = 25;
set @B = 15;
set @C = 35;
select cast(@A as int) + cast(@B as int)+ cast(@C as int) as Result ;
set @D = (select cast(@A as int) + cast(@B as int)+ cast(@C as int) as Result );
select @D as RESULT_IN_D

������ 1
� ������ ������� ���������� ����� ���������, � ������� ������ ����� ���� � 3, ��� ����� �� �������� ListPrice ����������������� �int.

SELECT SUBSTRING(Name, 1, 30) AS ProductName, ListPrice  
FROM Production.Product  
WHERE CAST(ListPrice AS int) LIKE '3%';

SELECT SUBSTRING(Name, 1, 30) AS ProductName, ListPrice  
FROM Production.Product  
WHERE CONVERT(int, ListPrice) LIKE '3%'; 

������ 2 ������������� ������� CAST � ������������ LIKE
� ��������� ������� ������� SalesYTD � ����� ������ money ������������� � ��� int, � ����� � ��� char(20) � ���, ����� ��� ����� ���� ������������ � ����������� LIKE

SELECT p.FirstName, p.LastName, s.SalesYTD, s.BusinessEntityID
FROM Person.Person AS p 
JOIN Sales.SalesPerson AS s 
    ON p.BusinessEntityID = s.BusinessEntityID
WHERE CAST(CAST(s.SalesYTD AS int) AS char(20)) LIKE '2%';
