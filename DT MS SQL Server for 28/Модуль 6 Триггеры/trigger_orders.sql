USE [AdventureWorks2008]
GO

CREATE TABLE Orders (
    OrderId INT NOT NULL,
    Price MONEY NOT NULL,
    Quantity INT NOT NULL,
    OrderDate DATETIME NOT NULL,
    Total AS Price * Quantity,
    ShippedDate AS DATEADD (DAY, 7, orderdate)
);

GO
CREATE VIEW view_AllOrders
    AS SELECT *
    FROM Orders;

GO
CREATE TRIGGER trigger_orders
    ON view_AllOrders INSTEAD OF INSERT
    AS BEGIN
        INSERT INTO Orders
        SELECT OrderId, Price, Quantity, OrderDate
        FROM inserted
END