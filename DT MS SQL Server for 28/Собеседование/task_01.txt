Hi Artem!

Please find list of warm-up question for the .NET interview:


1.Does C# support multiple-inheritance?
2.Who is a protected class-level variable available to?
3.Are private class-level variables inherited?
4.Describe the accessibility modifier �protected internal�.
5.What�s the top .NET class that everything is derived from?
6.What does the term immutable mean?
7.What�s the difference between System.String and System.Text.StringBuilder classes?
8.What�s the advantage of using System.Text.StringBuilder over System.String?
9.Can you store multiple data types in System.Array?
10.What�s the difference between the System.Array.CopyTo() and System.Array.Clone()?
11.How can you sort the elements of the array in descending order?
12.What�s the .NET collection class that allows an element to be accessed using a unique key?
13.What class is underneath the SortedList class?
14.Will the finally block get executed if an exception has not occurred?
15.What�s the C# syntax to catch any possible exception?
16.Can multiple catch blocks be executed for a single try statement?
17.Explain the three services model commonly know as a three-tier application
18.What is the syntax to inherit from a class in C#?
19.Can you prevent your class from being inherited by another class?
20.Can you allow a class to be inherited, but prevent the method from being over-ridden?
21.What�s an abstract class?
22.When do you absolutely have to declare a class as abstract?
23.What is an interface class?
24.Why can�t you specify the accessibility modifier for methods inside the interface?
25.Can you inherit multiple interfaces?
26.What happens if you inherit multiple interfaces and they have conflicting method names?
