﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;//для работы с файлом App.config - из System.configuration.dll
using System.Data.SqlClient;// из драйвера MS SQL Server-а
using System.Data.SqlTypes;//System.Data.dll

namespace MSServerTestConnetion
{
    class Program
    {
        static void Main(string[] args)
        {
            //string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            //string connectionString = @"Data Source=DESKTOP-V7KQ1U2\SQLEXPRESS;Initial Catalog=AdventureWorks2008;Integrated Security=True";
            string connectionString = @"Data Source=PC36-6-Z;Initial Catalog=db28pr6;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                StringBuilder sqltext = selectCustomers();
                //using (SqlCommand command = new SqlCommand("SELECT * FROM [db28pr5].[dbo].[CUSTOMERS]", con))
                using (SqlCommand command = new SqlCommand(sqltext.ToString(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<Customer> customers = new List<Customer>();
                    while (reader.Read())
                    {
                        int v1 = reader.GetInt32(0);
                        string nick = reader["NICK_NAME"].ToString();//reader.GetString(1);
                        string phone = reader["PHONE"].ToString();//reader.GetString(2);
                        string email = reader["EMAIL"].ToString();//DateTime datevalue = reader.GetDateTime(2);
                        string id_men = reader["ID_MEN"].ToString();
                        Customer customer = new Customer(id_men, nick, phone, email);
                        customer.Sname = reader["LNAME"].ToString();
                        customer.Lname = reader["NAME"].ToString();
                        int year;
                        Int32.TryParse(reader["ID_MEN"].ToString(),out year);
                        customer.Year = year;
                        customers.Add(customer);
                        Console.WriteLine("поля из выборки {0} {1} {2}",v1, nick, phone);
                        Console.WriteLine("создан customer: {0}",customer);
                    }
                }
                
                
            }
        }

        public static StringBuilder selectCustomers()
        {
            StringBuilder sqltext = new StringBuilder();
            sqltext.Append(" SELECT  [db28pr6].[dbo].[CUSTOMERS].ID,");
            sqltext.Append(" [db28pr6].[dbo].[CUSTOMERS].EMAIL, ");
            sqltext.Append(" [db28pr6].[dbo].[CUSTOMERS].NICK_NAME, ");
            sqltext.Append(" [db28pr6].[dbo].[CUSTOMERS].PHONE, ");
            sqltext.Append(" [db28pr6].[dbo].[CUSTOMERS].ID_MEN, ");
            sqltext.Append(" [db28pr6].[dbo].[MEN].LNAME, ");
            sqltext.Append(" [db28pr6].[dbo].[MEN].NAME, ");
            sqltext.Append(" [db28pr6].[dbo].[MEN].YEAR ");
            sqltext.Append(" FROM  ");
            sqltext.Append(" [db28pr6].[dbo].[CUSTOMERS] ");
            sqltext.Append(" LEFT JOIN [db28pr6].[dbo].[MEN] on  ");
            sqltext.Append(" [db28pr6].[dbo].[MEN].ID=[db28pr6].[dbo].[CUSTOMERS].ID_MEN ");

            return sqltext;
        }
    }
}
