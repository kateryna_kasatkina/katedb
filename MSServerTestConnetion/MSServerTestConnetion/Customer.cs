﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSServerTestConnetion
{
    public class Customer
    {
        protected Customer()
        {

        }
        public Customer(string id_men,string nick_name, string phone, string email)
        {
            this.id_men = id_men;
            this.nick_name = nick_name;
            this.phone = phone;
            this.email = email;
        }
        public override string ToString()
        {
            return String.Format("свдения о записи с Id={0} ({1},{2}): {3},{4},{5}.",Id_men,Lname,Sname,Nick_name,Phone,Email);
        }
        #region fields
        private string nick_name;

        public string Nick_name
        {
            get { return nick_name; }
            set { nick_name = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string id_men;

        public string Id_men
        {
            get { return id_men; }
            set { id_men = value; }
        }
        private string lname;

        public string Lname
        {
            get { return lname; }
            set { lname = value; }
        }
        private string sname;

        public string Sname
        {
            get { return sname; }
            set { sname = value; }
        }
        private int year;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }
        #endregion
    }
}
