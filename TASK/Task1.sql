USE MASTER
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'MyDB') DROP DATABASE MyDB
GO
CREATE DATABASE MyDB
GO
USE [MyDB]
GO
CREATE TABLE [DBO].[MyProducts]
(
[MyProduct_id]  INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
[name] [nvarchar](50) ,
[price] [MONEY] 
);
GO
CREATE TABLE [DBO].[MyClients]
(
[MyClient_id]  INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
[name] [nvarchar](50) ,
[phone] [nchar](20) ,
[Email] [nvarchar] (50) 
);
GO
CREATE TABLE [DBO].[MyOrders]
(
[MyOrder_id]  INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
[Description] [varchar] (250),
[OrdersDate] [Date],
[TotalCost] [Money],
[MyClient_id] [INT] FOREIGN KEY REFERENCES MyClients(MyClient_id)
);
GO 

CREATE TABLE [DBO].[MyOrdersPositions]
(
[MyOrdersPosition_id]  INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
[MyOrder_id] [INT] FOREIGN KEY REFERENCES MyOrders(MyOrder_id),
[MyProduct_id] [INT]FOREIGN KEY REFERENCES MyProducts(MyProduct_id),
[price] [Money],
[count] [int]
)

CREATE TABLE [DBO].[MyLog]
(
[Mylog_id]  INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
[Action] [INT],
[Tbl_Name] [varchar] (50),
[Id_Row] [INT],
[log_info] [varchar] (250) ,
[log_time] [date]
);


INSERT INTO [DBO].[MyProducts] ([name],[price]) VALUES (N'PEN',10.20);
INSERT INTO [DBO].[MyProducts] ([name],[price] ) VALUES (N'PENCIL',12.20);
INSERT INTO [DBO].[MyProducts] ([name],[price] ) VALUES (N'BOOK',112.20);
INSERT INTO [DBO].[MyProducts] ([name],[price] ) VALUES (N'JOURNAL',26.20);
INSERT INTO [DBO].[MyProducts] ([name],[price] ) VALUES (N'BOX',1.20);

SELECT
*
FROM [DBO].[MyProducts]


INSERT INTO [DBO].[MyClients] ([name],[phone],[Email]) VALUES (N'Alex',N'0994567897',N'email1@gmail.com');
INSERT INTO [DBO].[MyClients] ([name],[phone],[Email]) VALUES (N'Egor',N'0994567898',N'email2@gmail.com');
INSERT INTO [DBO].[MyClients] ([name],[phone],[Email]) VALUES (N'Tilda',N'0994567891',N'email3@gmail.com');
INSERT INTO [DBO].[MyClients] ([name],[phone],[Email]) VALUES (N'Olga',N'0994567892',N'email4@gmail.com');
INSERT INTO [DBO].[MyClients] ([name],[phone],[Email]) VALUES (N'Sonya',N'0994567893',N'email5@gmail.com');

SELECT
*
FROM [DBO].[MyClients]


INSERT INTO [DBO].[MyOrders] ([Description],[OrdersDate],[TotalCost],[MyClient_id]) VALUES(N'new customer','2017-02-06',61.00,2);
INSERT INTO [DBO].[MyOrders] ([Description],[OrdersDate],[TotalCost],[MyClient_id]) VALUES(N'old customer','2017-02-07',40.80,1);
INSERT INTO [DBO].[MyOrders] ([Description],[OrdersDate],[TotalCost],[MyClient_id]) VALUES(N'new customer','2017-02-08',336.60,4);
INSERT INTO [DBO].[MyOrders] ([Description],[OrdersDate],[TotalCost],[MyClient_id]) VALUES(N'old customer','2017-02-09',2.40,3);
INSERT INTO [DBO].[MyOrders] ([Description],[OrdersDate],[TotalCost],[MyClient_id]) VALUES(N'new customer','2017-02-10',26.20,5);

SELECT
*
FROM [DBO].[MyOrders]

INSERT INTO [DBO].[MyOrdersPositions] ([MyOrder_id],[MyProduct_id],[price],[count]) VALUES(1,2,12.20,5);
INSERT INTO [DBO].[MyOrdersPositions] ([MyOrder_id],[MyProduct_id],[price],[count]) VALUES(2,1,10.20,4);
INSERT INTO [DBO].[MyOrdersPositions] ([MyOrder_id],[MyProduct_id],[price],[count]) VALUES(3,3,112.20,3);
INSERT INTO [DBO].[MyOrdersPositions] ([MyOrder_id],[MyProduct_id],[price],[count]) VALUES(4,5,1.20,2);
INSERT INTO [DBO].[MyOrdersPositions] ([MyOrder_id],[MyProduct_id],[price],[count]) VALUES(5,4,26.20,1);

SELECT
*
FROM [DBO].[MyOrdersPositions]


INSERT INTO [DBO].[MyLog] ([Action],[Tbl_Name],[Id_Row],[log_info],[log_time]) VALUES (1,N'MyOrders',1,'STABLE','2017-02-10');
INSERT INTO [DBO].[MyLog] ([Action],[Tbl_Name],[Id_Row],[log_info],[log_time]) VALUES (2,N'MyOrders',2,'STABLE','2017-02-11');
INSERT INTO [DBO].[MyLog] ([Action],[Tbl_Name],[Id_Row],[log_info],[log_time]) VALUES (3,N'MyOrders',3,'STABLE','2017-02-12');
INSERT INTO [DBO].[MyLog] ([Action],[Tbl_Name],[Id_Row],[log_info],[log_time]) VALUES (4,N'MyOrders',4,'STABLE','2017-02-13');
INSERT INTO [DBO].[MyLog] ([Action],[Tbl_Name],[Id_Row],[log_info],[log_time]) VALUES (5,N'MyOrders',5,'STABLE','2017-02-14');

SELECT
*
FROM [DBO].[MyLog]


SELECT
[DBO].[MyOrders].[Description],
[DBO].[MyOrders].[OrdersDate],
[DBO].[MyOrders].[TotalCost],
[DBO].[MyOrdersPositions].[count],
[DBO].[MyOrdersPositions].[price],
[DBO].[MyOrdersPositions].[count]*[DBO].[MyOrdersPositions].[price] AS general_sum,
[DBO].[MyOrders].[MyClient_id],
[DBO].[MyClients].[name],
[DBO].[MyClients].[phone]
FROM [DBO].[MyOrders] 
JOIN [DBO].[MyClients] ON [DBO].[MyOrders].[MyClient_id]=[DBO].[MyClients].[MyClient_id]
JOIN [DBO].[MyOrdersPositions] ON [DBO].[MyOrdersPositions].[MyOrder_id]=[DBO].[MyOrders].[MyOrder_id];
