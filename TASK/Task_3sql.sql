USE AdventureWorks2012;

/*1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������ */
SELECT
[HumanResources].[EmployeePayHistory].[BusinessEntityID],
[HumanResources].[EmployeePayHistory].[PayFrequency],
[HumanResources].[Employee].[JobTitle]
FROM [HumanResources].[EmployeePayHistory] 
LEFT JOIN [HumanResources].[Employee]  ON [HumanResources].[EmployeePayHistory].BusinessEntityID=[HumanResources].[Employee].[BusinessEntityID]
WHERE [HumanResources].[EmployeePayHistory].[PayFrequency]=1;

SELECT
[HumanResources].[EmployeePayHistory].[BusinessEntityID],
[HumanResources].[EmployeePayHistory].[PayFrequency],
[HumanResources].[Employee].[JobTitle]
FROM [HumanResources].[EmployeePayHistory] 
LEFT JOIN [HumanResources].[Employee]  ON [HumanResources].[EmployeePayHistory].BusinessEntityID=[HumanResources].[Employee].[BusinessEntityID]
WHERE [HumanResources].[EmployeePayHistory].[PayFrequency]=2;

/*2. ������ �������,� ������� �������� ����������, �� ����������, ������������ ��� � ����� */
SELECT DISTINCT d.Name
FROM [HumanResources].[Employee] as e 
INNER JOIN [HumanResources].[EmployeePayHistory] as eph ON (e.BusinessEntityID = eph.BusinessEntityID) 
INNER JOIN [HumanResources].[EmployeeDepartmentHistory] as edh ON (e.BusinessEntityID = edh.BusinessEntityID) 
INNER JOIN [HumanResources].[Department] as d ON (edh.DepartmentID = d.DepartmentID) 
WHERE eph.PayFrequency = 1;


/*3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� ������� */
SELECT
p.[BusinessEntityID] as id,
p.[FirstName] as name,
p.[LastName]  as l_name,
eph.[Rate],
eph.[PayFrequency],
e.[JobTitle]
FROM [Person].[Person] as p
JOIN [HumanResources].[EmployeePayHistory] as eph ON (p.[BusinessEntityID]=eph.[BusinessEntityID])
JOIN [HumanResources].[Employee] as e ON (e.BusinessEntityID=p.BusinessEntityID);




use [NORTHWND];
/*1. ������� ��������, � ������� ���� ������ */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName]
FROM Customers
WHERE [Customers].[CustomerID] IN 
(
SELECT DISTINCT
[Orders].[CustomerID]
FROM [Orders]
);
/*2. ������� ���� ��������, � ������� ��� �������� ������*/
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName]
FROM Customers
WHERE [Customers].[CustomerID] NOT IN 
(
SELECT DISTINCT
[Orders].[CustomerID]
FROM [Orders]
);
/*3. ������� ��������, � ������� ������ ���� ����� */
SELECT
MyTable.[CustomerID],
MyTable.[number_of_orders]
FROM
(SELECT 
COUNT([Orders].[CustomerID]) AS number_of_orders,
[Orders].[CustomerID]
FROM [Orders]
JOIN [Customers] ON [Customers].[CustomerID]=[Orders].[CustomerID]
GROUP BY [Orders].[CustomerID]
) AS MyTable
WHERE MyTable.[number_of_orders]=1;

/*4. ������� ��������, � ������� ����� ���� ������� */
SELECT
MyTable.[CustomerID],
MyTable.[number_of_orders]
FROM
(SELECT 
COUNT([Orders].[CustomerID]) AS number_of_orders,
[Orders].[CustomerID]
FROM [Orders]
JOIN [Customers] ON [Customers].[CustomerID]=[Orders].[CustomerID]
GROUP BY [Orders].[CustomerID]
) AS MyTable
WHERE MyTable.[number_of_orders]>2;

/*5. ������� ���� ��������, � ������� �� ����� Fax */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName]
FROM [Customers]
WHERE [Customers].[Fax] IS NULL;

/*6. ������� ���� ��������, � ������� ����� ������ */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Region]
FROM [Customers]
WHERE [Customers].[Region] IS  NOT NULL;

/*7. ������� ���� ��������, � ������� PostalCode ���������� � ���� */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[PostalCode]
FROM [Customers]
WHERE [Customers].[PostalCode] LIKE '0%';


/*8. ������� ���� ��������, � ������� PostalCode ������� 5 �������� */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[PostalCode]
FROM [Customers]
WHERE [Customers].[PostalCode] LIKE '_%_%_%_%_%_%';

/*9. ������� ���� ��������, � ������� PostalCode ���������� � ���� � ������� 5 �������� */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[PostalCode]
FROM [Customers]
WHERE [Customers].[PostalCode] LIKE '0%_%_%_%_%_%';


/*10. ������� ���� ��������, � ������� Phone ������������� �� 4 */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Phone]
FROM [Customers]
WHERE [Customers].[Phone] LIKE '%4';

/*11. ������� ���� ��������, � ������� Phone �������� ������ */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Phone]
FROM [Customers]
WHERE [Customers].[Phone] LIKE '%(%';

/*12. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 1 ������ */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Phone]
FROM [Customers]
WHERE [Customers].[Phone] LIKE '%(_)%';

/*13. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 3 �������*/
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Phone]
FROM [Customers]
WHERE [Customers].[Phone] LIKE '%(___)%';

/*14. ������� ���� ��������, � ������� Phone ����� ������� ��� */

/*15. ������� ���� ��������, � ������� Phone ����� ������� ��� � ������ �������� ���� ������ (����� ������� ������� �� ��������) */
SELECT 
[Customers].[CustomerID],
[Customers].[ContactName],
[Customers].[Phone]
FROM [Customers]
WHERE [Customers].[Phone] LIKE '%(_%_%_%)%';

/*16. ��������� ���������� �������� � ������ ������� */
SELECT
COUNT([Customers].[Region]) AS number_of_customers,
[Customers].[Region]
FROM Customers
GROUP BY Region
HAVING [Customers].[Region] IS NOT NULL;


/*17. ������� ������ �������� �� ���� �������, � ������� �� ������ ����� */
--REGION WITH MAX EMOUNT OF CUSTOMERS
SELECT TOP 1
COUNT([Customers].[Region]) AS number_of_customers,
[Customers].[Region]
FROM Customers
GROUP BY Region
HAVING [Customers].[Region] IS NOT NULL
ORDER BY number_of_customers DESC

-- SHOW CUSTOMERS FROM SELECTED REGION
SELECT
MyTable.[Region],
[Customers].[CustomerID],
[Customers].[ContactName]
FROM (SELECT TOP 1
COUNT([Customers].[Region]) AS number_of_customers,
[Customers].[Region]
FROM Customers
GROUP BY Region
HAVING [Customers].[Region] IS NOT NULL
ORDER BY number_of_customers DESC) AS MyTable
JOIN [Customers] ON MyTable.[Region]=[Customers].[Region];

/*18. ������� ������ ���������� (Employee), ������� ������� � ��������� �� ���� �������, ��� �������� ������ ����� */
WITH region_statistics as (
	select Region, COUNT(CustomerID) as cntCustomers
	from Customers
	where Region IS NOT NULL
	group by Region
)
SELECT EmployeeID, FirstName, LastName
FROM Employees 
WHERE EmployeeID in (
	SELECT e.EmployeeID
	FROM Customers as c
		 INNER JOIN Orders as o ON c.CustomerID = o.CustomerID
		 INNER JOIN Employees as e ON o.EmployeeID = e.EmployeeID
	WHERE c.Region in (
		SELECT Region
		FROM region_statistics
		WHERE cntCustomers = (select min(cntCustomers) from region_statistics)
	)
)