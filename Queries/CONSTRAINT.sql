use db28pr6;
go
CREATE TABLE [dbo].[EmployeesToDepartments]
(
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	id_Emloyees INT  NOT NULL,
	id_Departments INT  NOT NULL
);

ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT PK_EmployeesToDepartments PRIMARY KEY (ID);
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT FK_EmployeesToDepartments_id_Emloyees 
FOREIGN KEY (id_Emloyees) REFERENCES [dbo].EMPLOYEES(ID);
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT FK_EmployeesToDepartments_id_Emloyees 
FOREIGN KEY (id_Departments) REFERENCES [dbo].DEPARTMENTS(ID);
go
insert into [dbo].[EmployeesToDepartments] (id_Emloyees,id_Departments) values (1,1);
insert into [dbo].[EmployeesToDepartments] (id_Emloyees,id_Departments) values (1,1);
insert into [dbo].[EmployeesToDepartments] (id_Emloyees,id_Departments) values (1,1);
--insert into [dbo].[EmployeesToDepartments] (id_Emloyees,id_Departments) values (1,1);
--insert into [dbo].[EmployeesToDepartments] (id_Emloyees,id_Departments) values (1,1);
use [db28pr6]
select 
*
from
[dbo].[DEPARTMENTS] dep
left join [dbo].[EmployeesToDepartments] e2d
on dep.ID=e2d.id_Departments
left join EMPLOYEES emp
on emp.id=e2d.id_Emloyees
left join MEN m on m.id=emp.ID_MEn