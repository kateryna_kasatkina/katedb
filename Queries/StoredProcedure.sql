use NORTHWIND;

alter table [Customers]
add [CompanyName1] nvarchar(100)  null;


USE [NORTHWIND]
ALTER TABLE [dbo].[Customers] DROP COLUMN [CompanyName1]

select
*
from Employees

--if id was identity(autoincrement) for forced inserting employee_id 
USE [NORTHWIND]
begin transaction
set Identity_insert [dbo].[Employees] on;
insert into [Employees] (EmployeeID,LastName,FirstName) values(0,N'Kasatkina',N'Kateryna');
set identity_insert [dbo].[Employees] off;
commit

USE [NORTHWIND]
declare @Parametr int--declaring varioble
set @Parametr=0;--set value to variable

select
[Employees].[EmployeeID]
from [dbo].[Employees]
where[dbo].[Employees].[EmployeeID]=@Parametr

--������ �������������� �����
declare @A varchar(2)
declare @B varchar(2)
declare @C varchar(2)
declare @D varchar(2)
set @A=25;
set @B=15;
set @C=35;
--select cast(@B as int)+ cast(@C as int) as Result
set @D=(select cast(@A as int)+ cast(@B as int)+cast(@C as int) as Result);
select @D as result_in_d

--storage procedure!!!!!!!!!!! if void => procedure if return something=>function
use [NORTHWIND];
go
create procedure PROC_FOR_TEST
(
--������� ���������
@p1 int,
@p2 int,
@out_sum varchar(200) out--�������� ��������
)
as 
begin
select @out_sum=@p1+@p2
end 

--using storage procedure
declare @my_sum as nvarchar(200)
execute dbo.PROC_FOR_TEST 1,2, @my_sum output
select @my_sum as MYSUM