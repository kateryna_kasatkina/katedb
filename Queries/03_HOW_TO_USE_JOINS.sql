USE [Northwind]
GO
/*
SELECT
*
FROM
[DBO].[Customers]
*/
--����������� �������� � ������� � ��������� ��� �������
/*
SELECT
[DBO].[Orders].[OrderDate],
[DBO].[Orders].[OrderID],
[DBO].[Orders].[CustomerID],
[DBO].[Customers].[ContactName],
[DBO].[Orders].[EmployeeID],
[DBO].[Employees].[FirstName]
FROM [DBO].[Orders]
LEFT JOIN [DBO].[Customers] ON [DBO].[Orders].[CustomerID]=[DBO].[Customers].[CustomerID]
LEFT JOIN [DBO].[Employees] ON [DBO].[Orders].[EmployeeID]=[DBO].[Employees].[EmployeeID]
*/
--���� �� � ������������ ������ � 1996 ����?
--1)������ ������� 1996 ����
--2)������ ������������, ������� ���� � ������ �������
/*1)
SELECT
[DBO].[Orders].[OrderDate],
[DBO].[Orders].[OrderID],
[DBO].[Orders].[CustomerID]
FROM [DBO].[Orders]
WHERE [DBO].[Orders].OrderDate >= '1996-01-01' AND [DBO].[Orders].OrderDate < '1997-01-01'
*/
SELECT * FROM [DBO].[Customers]
WHERE [DBO].[Customers].[CustomerID] IN 
(SELECT [DBO].[Orders].[CustomerID] FROM [DBO].[Orders]
WHERE [DBO].[Orders].OrderDate >= '1996-01-01' AND [DBO].[Orders].OrderDate < '1997-01-01')

--������ �������, ������� ����������� � 1996 ����
select 
[dbo].[Order Details].ProductID,[dbo].Products.ProductName,
[dbo].[Order Details].OrderID,
[dbo].[Order Details].UnitPrice,[dbo].[Order Details].Quantity
from
[dbo].[Order Details] left join [dbo].Products 
on [dbo].[Order Details].ProductID = [dbo].Products.ProductID
where 
[dbo].[Order Details].OrderID
in (SELECT [DBO].[Orders].OrderID 
    FROM [DBO].[Orders]
    WHERE [DBO].[Orders].OrderDate >= '1996-01-01' 
	  AND [DBO].[Orders].OrderDate < '1997-01-01')