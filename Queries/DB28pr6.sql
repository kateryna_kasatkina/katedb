use master
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr6') DROP DATABASE db28pr6
go
CREATE DATABASE db28pr6
go
use [db28pr6]
go
CREATE TABLE [dbo].[CUSTOMERS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NICKNAME] [nvarchar](50) NULL,
	[PHONE] [nchar](20) NOT NULL,
	[EMAIL] [nchar](50) NOT NULL,
	[ID_MEN] [int] NULL
 )
go
CREATE TABLE [dbo].[POSITIONS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NULL,
	[ROLE] [nchar](20) NOT NULL
 )
go
CREATE TABLE [dbo].[MEN](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[LNAME] [nvarchar](50) NOT NULL,
	[YEAR] [int] NOT NULL
 )
go
CREATE TABLE [dbo].[EMPLOYEES](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[ID_MEN] [int] NOT NULL,
	[CODE] [nvarchar](50) NULL,
	[ID_POSITIONS] [int] NOT NULL
 )
 go
CREATE TABLE [dbo].[MANAGERS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_EMPLOYEES] [int] NULL,
	[ID_POSITIONS] [int] NULL
 )
 go
CREATE TABLE [dbo].[DEPARTMENTS](
	[ID] [int] IDENTITY(1,1) primary key NOT NULL,
	[NAME] [nvarchar](50) NOT NULL,
	[ID_MANAGER] [int] NULL
 )

 go
 create table [dbo].[CLIENTS](
[CLIENTS_ID ][int]IDENTITY(1,1) primary key NOT NULL,
[CLIENTS_NAME][nvarchar](50) NOT NULL,
[CLIENTS_PHONE][nvarchar](20) NOT NULL,
[CLIENTS_MAIL][nvarchar](50) NOT NULL,
)

go
create table [dbo].[PRODUCTS](
[PRODUCTS_ID ][int]IDENTITY(1,1) primary key NOT NULL,
[PRODUCTS_NAME][nvarchar](50) NOT NULL,
[PRICE][int] NOT NULL,
)


 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES (N'�����������');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('It-dev');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('It-QA');
 INSERT INTO [dbo].[DEPARTMENTS] (NAME) VALUES ('Accounters');

 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'�������', N'������', 1991);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'����������', N'������', 1985);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'����', N'������', 1976);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'����', N'������', 1995);
 INSERT INTO [dbo].[MEN] (NAME, LNAME, YEAR) VALUES (N'�����', N'��������', 1988);

 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'���������');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES ('C# Junior Developer');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES ('C# Developer');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES ('C# Senior Developer');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'Team Leader');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'Manual QA Engineer');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'Automation QA');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'���� ��������');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'CEO');
 INSERT INTO [dbo].[POSITIONS] (ROLE) VALUES (N'CTO');


go
insert into[dbo].[CLIENTS] ([CLIENTS_NAME],[CLIENTS_PHONE],[CLIENTS_MAIL])
values (N'������ ����', '1133456678', 'ivanov@gmail.com');
insert into[dbo].[CLIENTS] ([CLIENTS_NAME],[CLIENTS_PHONE],[CLIENTS_MAIL])
values (N'������� �����', '5533456678', 'petrova@gmail.com');
insert into[dbo].[CLIENTS] ([CLIENTS_NAME],[CLIENTS_PHONE],[CLIENTS_MAIL])
values (N'Cbljhjdf �����', '55334111678', 'sidorova@gmail.com');

go
insert into[dbo].[PRODUCTS] ([PRODUCTS_NAME],[PRICE]) values ( 'book', 200);
insert into[dbo].[PRODUCTS] ([PRODUCTS_NAME],[PRICE]) values ( 'pen', 20);
insert into[dbo].[PRODUCTS] ([PRODUCTS_NAME],[PRICE]) values ( 'pencil', 30);
insert into[dbo].[PRODUCTS] ([PRODUCTS_NAME],[PRICE]) values ( 'table', 366);
insert into[dbo].[PRODUCTS] ([PRODUCTS_NAME],[PRICE]) values ( 'chair', 200);

 use [db28pr6]
go
SELECT
	*
FROM
[dbo].[DEPARTMENTS]

go
SELECT
	*
FROM
[dbo].[MEN]
go
SELECT
[dbo].[PRODUCTS].[PRICE],
[dbo].[PRODUCTS].[PRODUCTS_NAME],
[dbo].[PRODUCTS].[PRODUCTS_ID ]
FROM
[dbo].[PRODUCTS]


--select 
--[dbo].[CLIENTS].[CLIENTS_ID ],
--[dbo].[CLIENTS].[CLIENTS_NAME],
--[dbo].[CLIENTS].[CLIENTS_PHONE],
--[dbo].[CLIENTS].[CLIENTS_MAIL]
--from [dbo].[CLIENTS]