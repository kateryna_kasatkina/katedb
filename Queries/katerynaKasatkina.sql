/*1. �������� �������, ��������� ������� ��������� ���� � �����������
 ��� ��������������� ���� ���� ������ (AdventureWorks ��� Northwind). */
use AdventureWorks2008;
CREATE TABLE [dbo].[USERS]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [USER_NAME] [NVARCHAR] (100)
)

CREATE TABLE [dbo].[ROLES]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [ROLE_NAME] [NVARCHAR](100),
  [RULE] [NCHAR]
)

CREATE TABLE [dbo].[USERS_TO_ROLES]
(
  [ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
  [USER_ID] [INT],
  [ROLE_ID] [INT],
  FOREIGN KEY (USER_ID) REFERENCES USERS(ID),
  FOREIGN KEY (ROLE_ID) REFERENCES ROLES(ID)
)

CREATE TABLE [dbo].[MYLOGS]
(
  [MYLOGS_ID] int IDENTITY(1,1) PRIMARY KEY ,
  [ACTIONN] [NCHAR](100),
  [TBL_NAME] [NVARCHAR](100),
  [ID_ROW] [INT],
  [LOG_INFO] [NVARCHAR](100),
  [LOG_TIME] [DATE]
)

/*2. �������� �������� ������ ���� ������ my_safety_create_log_trigger, 
������� ��������� �� ������� CREATE_TABLE, my_safety_drop_log_trigger (DROP TABLE),
 my_safety_alter_log_trigger (ALTER TABLE). */

   GO
   CREATE TRIGGER my_safety_create_log_trigger
   ON DATABASE 
   FOR CREATE_TABLE
   AS 
   PRINT N'��������� �������� CREATE_TABLE' 
   ROLLBACK


   GO
   CREATE TRIGGER my_safety_drop_log_trigger
   ON DATABASE 
   FOR DROP_TABLE
   AS 
   PRINT N'��������� �������� DROP_TABLE' 
   ROLLBACK;

   GO
   CREATE TRIGGER my_safety_alter_log_trigger
   ON DATABASE 
   FOR ALTER_TABLE
   AS 
   PRINT N'��������� �������� ALTER_TABLE' 
   ROLLBACK;


  /*3. ������ ������� ������ ��������� ������ � ������� ����� MYLOGS. 
  ��������, ������� my_safety_create_log_trigger ��������� ������� ���: ACTION=>CREATE_TABLE,
  TBL_NAME=>NOTDEF, ID_ROW=>-1, LOG_INFO=> ��������� �������� CREATE_TABLE, LOG_TIME=>getdate() */


GO
   CREATE TRIGGER my_safety_create_log_trigger1
   ON DATABASE 
   FOR CREATE_TABLE
   AS 
   BEGIN
	   SET CONCAT_NULL_YIELDS_NULL ON

	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Create table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� CREATE_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
	   SET CONCAT_NULL_YIELDS_NULL OFF   
   END

   GO
   CREATE TRIGGER my_safety_drop_log_trigger1
   ON DATABASE 
   FOR DROP_TABLE
   AS 
   BEGIN
	   SET CONCAT_NULL_YIELDS_NULL ON

	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Drop table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� DROP_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
	   SET CONCAT_NULL_YIELDS_NULL OFF   
   END


   GO
   CREATE TRIGGER my_safety_alter_log_trigger1
   ON DATABASE 
   FOR ALTER_TABLE
   AS 
   BEGIN
	   SET CONCAT_NULL_YIELDS_NULL ON

	   DECLARE @LOG_TIME DATE;
	   SET @LOG_TIME=getdate();

	   DECLARE @action NCHAR(100);
	   SET @action=N'Alter table';

	   DECLARE @table_name NVARCHAR(100);
	   SET @table_name=N'NOTDEF';

	   DECLARE @log_info NVARCHAR(100);
	   SET @log_info=N'��������� �������� ALTER_TABLE';

	   INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@log_info,@LOG_TIME)
	   SET CONCAT_NULL_YIELDS_NULL OFF   
   END

   /*���� III. �������� 
��� ������ �� ��������� ������ �������� ������� ���������������� ��������: users_log_trigger, roles_log_trigger, users_to_roles_log_trigger. 
������ �� ��������� ������� ���������� � ������� MYLOGS 
��������, ������� roles_log_trigger ������� ������ ����:  
ACTION=>U,  
TBL_NAME=>ROLES,  
ID_ROW=>12,  
LOG_INFO=> ����: ID=12,NAME=TEST_ROLE,RULE=UID �����: ID=12,NAME=TEST_ROLE,RULE=U, 
LOG_TIME=>getdate() */


GO
   CREATE TRIGGER Myusers_log_trigger
   ON users
   FOR UPDATE
    AS 
   BEGIN
	DECLARE @log_info_ID_NEW INT,@log_info_ID_OLD INT,@log_info_USER_NAME_NEW NVARCHAR(100),@log_info_USER_NAME_OLD NVARCHAR(100);
	DECLARE @LOG_TIME DATE;SET @LOG_TIME=getdate();
	DECLARE @action NCHAR(100);SET @action=N'UPDATE table USERS';
	DECLARE @table_name NVARCHAR(100);SET @table_name=N'USERS';
	DECLARE @str varchar(255);
	DECLARE CUR1 CURSOR FOR
		  SELECT
		  ID,
		  [USER_NAME]
		  FROM inserted
	DECLARE CUR2 CURSOR FOR
		  SELECT
		  ID,
		  [USER_NAME]
		  FROM deleted
	OPEN CUR1
	OPEN CUR2
	FETCH NEXT FROM CUR1 INTO   @log_info_ID_NEW, @log_info_USER_NAME_NEW
    FETCH NEXT FROM CUR2 INTO   @log_info_ID_NEW, @log_info_USER_NAME_OLD

	SET @str=N'old:'+@log_info_USER_NAME_OLD+N'new:'+@log_info_USER_NAME_NEW;
	INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@str,@LOG_TIME)
   END
	CLOSE CUR1
	CLOSE CUR2
	DEALLOCATE CUR1
	DEALLOCATE CUR2


	select
	*
	from USERS

	insert into USERS ([USER_NAME]) values(N'Kate5');

--	UPDATE table_name
--SET column1 = value1, column2 = value2, ...
--WHERE condition;

	update USERS
	set [USER_NAME]=N'Kate22'
	Where [USER_NAME]=N'Kate11';





	GO
   CREATE TRIGGER MyRoles_log_trigger
   ON roles
   FOR UPDATE
    AS 
   BEGIN
	DECLARE @log_info_role_NAME_NEW NVARCHAR(100),@log_info_role_NAME_OLD NVARCHAR(100);
	DECLARE @LOG_TIME DATE;SET @LOG_TIME=getdate();
	DECLARE @action NCHAR(100);SET @action=N'UPDATE table ROLES';
	DECLARE @table_name NVARCHAR(100);SET @table_name=N'ROLES';
	DECLARE @str varchar(255);
	DECLARE CUR1 CURSOR FOR
		  SELECT
		  [ROLE_NAME]
		  FROM inserted
	DECLARE CUR2 CURSOR FOR
		  SELECT
		  [ROLE_NAME]
		  FROM deleted
	OPEN CUR1
	OPEN CUR2
	FETCH NEXT FROM CUR1 INTO  @log_info_role_NAME_NEW
    FETCH NEXT FROM CUR2 INTO  @log_info_role_NAME_OLD
	SET @str=N'old:'+@log_info_role_NAME_OLD+N'new:'+@log_info_role_NAME_NEW;
	INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@str,@LOG_TIME)
   END
	CLOSE CUR1
	CLOSE CUR2
	DEALLOCATE CUR1
	DEALLOCATE CUR2

	
	select
	*
	from ROLES

	insert into ROLES([ROLE_NAME]) values(N'user4');

	update ROLES
	set [ROLE_NAME]=N'adm1'
	Where [ROLE_NAME]=N'user4';

    select 
	*
	from MYLOGS

	GO
   CREATE TRIGGER MyUsers_to_roles_log_trigger
   ON USERS_TO_ROLES
   FOR UPDATE
    AS 
   BEGIN
	DECLARE @role_ID_NEW INT,@role_ID_OLD INT;
	DECLARE @USER_ID_NEW INT,@USER_ID_OLD INT;
	DECLARE @LOG_TIME DATE;SET @LOG_TIME=getdate();
	DECLARE @action NCHAR(100);SET @action=N'UPDATE table USERS_TO_ROLES';
	DECLARE @table_name NVARCHAR(100);SET @table_name=N'USERS_TO_ROLES';
	DECLARE @str varchar(255);
	DECLARE @old varchar(10);SET @old=N'old:';
	DECLARE @new varchar(10);SET @new=N'new:';
	DECLARE CUR1 CURSOR FOR
		  SELECT
		  [ROLE_ID],
		  [USER_ID]
		  FROM inserted
	DECLARE CUR2 CURSOR FOR
		  SELECT
		  [ROLE_ID],
		  [USER_ID]
		  FROM deleted
	OPEN CUR1
	OPEN CUR2
	FETCH NEXT FROM CUR1 INTO  @role_ID_NEW,@role_ID_OLD
    FETCH NEXT FROM CUR2 INTO  @USER_ID_NEW,@USER_ID_OLD 
	SET @str=@role_ID_OLD+@USER_ID_OLD+@role_ID_NEW+@USER_ID_NEW;
	INSERT INTO MYLOGS (ACTIONN,TBL_NAME,ID_ROW,LOG_INFO,LOG_TIME) VALUES (
	   @action,@table_name,1,@str,@LOG_TIME)
    END
	CLOSE CUR1
	CLOSE CUR2
	DEALLOCATE CUR1
	DEALLOCATE CUR2

	insert into USERS_TO_ROLES([USER_ID],[ROLE_ID]) values(2,1);

	SELECT
	*
	FROM USERS_TO_ROLES

	update USERS_TO_ROLES
	set [ROLE_iD]=1
	Where [ROLE_iD]=2;

	select
	*
	from MYLOGS
