--https://docs.microsoft.com/en-us/sql/t-sql/language-elements/case-transact-sql
--https://docs.microsoft.com/en-us/sql/t-sql/language-elements/coalesce-transact-sql
--https://docs.microsoft.com/en-us/sql/t-sql/language-elements/nullif-transact-sql

use AdventureWorks2008;
select 
ProductNumber,
ProductLine,
Name
from Production.Product
order by ProductNumber


select
	ProductNumber, Category=
	Case ProductLine
	When 'R' then 'Road'
	when 'M' then 'Mountain'
	when 'T' then 'Touring'
	when 'S' then 'other sale item'
	else 'not for sale'
	end,
	Name
from Production.Product
order by ProductNumber


select
ProductID,
MakeFlag,
FinishedGoodsFlag,
'Null if Equal'=
	case
	when MakeFlag=FinishedGoodsFlag then null
	else MakeFlag
	end
from Production.Product
where ProductID<10;


--nullif Returns a null value if the two specified expressions are equal.
select
ProductID,
MakeFlag,
FinishedGoodsFlag,
	NULLIf(MakeFlag,FinishedGoodsFlag) as 'Null if Equal'
from Production.Product
where ProductID<10;


--������ 3 ������������� ������� ISNULL, ������� �������� �������� NULL ��������� ���������� ���������. ���������: ISNULL ( check_expression , replacement_value )
USE AdventureWorks2008;
GO
SELECT AVG(ISNULL(Weight, 50))
FROM Production.Product;
GO
	SELECT Description, DiscountPct, MinQty, ISNULL(MaxQty, 0.00) AS 'Max Quantity'
FROM Sales.SpecialOffer;

--������ 4 ������������� COALSCE, ������� ���������� ������ ��������� �� ������ ����������, �� ������ NULL.
USE AdventureWorks2008;
GO
SELECT Name, Class, Color, ProductNumber,
COALESCE(Class, Color, ProductNumber) AS FirstNotNull
FROM Production.Product ;
GO




