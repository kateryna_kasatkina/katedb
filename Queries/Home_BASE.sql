use master 
IF EXISTS(SELECT 'True' FROM sys.databases WHERE name LIKE 'db28pr6h') 
DROP DATABASE db28pr6h 
go
CREATE DATABASE  db28pr6h
go
use [db28pr6h]
go
CREATE TABLE [dbo].[CLIENTS]
(
 [CLIENTS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [CLIENTS_NAME] [nvarchar](50) NOT NULL, -- � ������������� NULL
 [CLIENTS_PHONE] [nchar](20) NOT NULL,
 [CLIENTS_MAIL] [nvarchar](50) NOT NULL
)
go
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'������ ����',N'+3809789678',N'ivrov@e-mail.com');
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'������� �����',N'+3800999678',N'petr@e-mail.com');
insert into [dbo].[CLIENTS] (CLIENTS_NAME,CLIENTS_PHONE, CLIENTS_MAIL) values (N'������� ����',N'+3809088678',N'sudrov@e-mail.com');

go

CREATE TABLE [dbo].[BANKS]
(
 [BANKS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [BANKS_NAME] [nvarchar](50) NOT NULL,
 [REGION_INFO] [nvarchar](100) NULL
)
go
insert into [dbo].[BANKS] (BANKS_NAME,REGION_INFO) values (N'��������', N'�������, �������');
insert into [dbo].[BANKS] (BANKS_NAME,REGION_INFO) values (N'������', N'�������, ����');
insert into [dbo].[BANKS] (BANKS_NAME) values (N'��������');

go

CREATE TABLE [dbo].[ACCOUNTS]
(
 [ACCOUNTS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [DESCRIPTION] [nvarchar](100) NULL,
 [BANKS_ID] [int] NOT NULL,
 [ACCOUNT_SUM] [int] NOT NULL, 
 [ACCOUNT] [int] NOT NULL
)
go
ALTER TABLE [dbo].[ACCOUNTS] ADD CONSTRAINT FK_ACCOUNTS_BANKS_ID 
FOREIGN KEY (BANKS_ID) REFERENCES [dbo].[BANKS](BANKS_ID);
go
insert into [dbo].[ACCOUNTS] (DESCRIPTION,BANKS_ID,ACCOUNT_SUM,ACCOUNT) values (N'������� ����',1,300,858575753);
insert into [dbo].[ACCOUNTS] (DESCRIPTION,BANKS_ID,ACCOUNT_SUM,ACCOUNT) values (N'������������� ����',3,0,04845753);
insert into [dbo].[ACCOUNTS] (BANKS_ID,ACCOUNT_SUM,ACCOUNT) values (2,1000,937375753);
insert into [dbo].[ACCOUNTS] (BANKS_ID,ACCOUNT_SUM,ACCOUNT) values (1,340,363375753);
--insert into [dbo].[ACCOUNTS] (BANKS_ID,ACCOUNT_SUM,ACCOUNT) values (60,340,363375753); 60 - ����������� ������
go
CREATE TABLE [dbo].[ACCOUNTS_TO_CLIENTS]
(
 [ACCOUNTS_TO_CLIENTS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [ACCOUNTS_ID] [int] NOT NULL,
 [CLIENTS_ID] [int] NOT NULL, 
 [INDICATION] [int] NULL
)
go
ALTER TABLE [dbo].[ACCOUNTS_TO_CLIENTS] ADD CONSTRAINT FK_ACCOUNTS_TO_CLIENTS_ACCOUNTS_ID 
FOREIGN KEY (ACCOUNTS_ID) REFERENCES [dbo].[ACCOUNTS](ACCOUNTS_ID);
ALTER TABLE [dbo].[ACCOUNTS_TO_CLIENTS] ADD CONSTRAINT FK_ACCOUNTS_TO_CLIENTS_CLIENTS_ID
FOREIGN KEY (CLIENTS_ID) REFERENCES [dbo].[CLIENTS](CLIENTS_ID);
go 
insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_ID, CLIENTS_ID) values (1,3);
insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_ID, CLIENTS_ID) values (2,3);
--insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_ID, CLIENTS_ID) values (2,10); ��� ������
--insert into [dbo].[ACCOUNTS_TO_CLIENTS] (ACCOUNTS_ID, CLIENTS_ID) values (10,1); -����� �� �������
go
Create TABLE [dbo].[PRODUCTS]
(
 [PRODUCT_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [PRODUCT_NAME] [nvarchar](50) NOT NULL,
 [PRODUCT_PRICE] [int] NOT NULL
)
go
insert into [dbo].[PRODUCTS] (PRODUCT_NAME, PRODUCT_PRICE) 
values (N'�����',12);
insert into [dbo].[PRODUCTS] (PRODUCT_NAME, PRODUCT_PRICE) 
values (N'������',10);
insert into [dbo].[PRODUCTS] (PRODUCT_NAME, PRODUCT_PRICE) 
values (N'������',25);
insert into [dbo].[PRODUCTS] (PRODUCT_NAME, PRODUCT_PRICE) 
values (N'���',150);
insert into [dbo].[PRODUCTS] (PRODUCT_NAME, PRODUCT_PRICE) 
values (N'������',100);

go

Create TABLE [dbo].[ORDERS]
(
 [ORDERS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [DESCRIPTION] [nvarchar](200) NULL, 
 [ORDERS_DATE] [Date] NOT NULL,
 [TOTAL_COSTS][int] NOT NULL,
 [CLIENTS_ID][int] NOT NULL
)
go
ALTER TABLE [dbo].[ORDERS] ADD CONSTRAINT FK_ORDERS_ACCOUNTS_CLIENTS_ID 
FOREIGN KEY (CLIENTS_ID) REFERENCES [dbo].[CLIENTS](CLIENTS_ID);
go
insert into [dbo].[ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'������� �����', '2016-11-11',100,1);
insert into [dbo].[ORDERS] (DESCRIPTION,ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values (N'���������� �����', '2016-12-10',200,3);
insert into [dbo].[ORDERS] (ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values ('2016-12-11',300,2);
insert into [dbo].[ORDERS] (ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values ('2016-12-12',200,3);
--insert into [dbo].[ORDERS] (ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values ('2016-45-12',200,3); ������������ ����
--insert into [dbo].[ORDERS] (ORDERS_DATE,TOTAL_COSTS,CLIENTS_ID) values ('2016-45-12',200,60); ������������ id �������

go
 Create TABLE [dbo].[ORDERS_POSITIONS]
(
 [ORDERS_POSITION_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [ORDERS_ID][int] NOT NULL,
 [PRODUCTS_ID][int] NOT NULL,
 [PRICE][int] NOT NULL,
 [ITEM_COUNT][int] NOT NULL
)
go
ALTER TABLE [dbo].[ORDERS_POSITIONS] ADD CONSTRAINT FK_ORDERS_POSITIONS_ORDERS_ID
FOREIGN KEY (ORDERS_ID) REFERENCES [dbo].[ORDERS](ORDERS_ID);
ALTER TABLE [dbo].[ORDERS_POSITIONS] ADD CONSTRAINT FK_ORDERS_POSITIONS_PRODUCTS_ID
FOREIGN KEY (PRODUCTS_ID) REFERENCES [dbo].[PRODUCTS](PRODUCT_ID);
go
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,1,100,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,2,10,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,3,40,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,4,70,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (1,5,40,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (2,1,100,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (2,2,10,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,3,40,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,4,70,20);
insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,5,40,20);
--insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (31,4,70,20);
--insert into [dbo].[ORDERS_POSITIONS](ORDERS_ID, PRODUCTS_ID, PRICE, ITEM_COUNT) values (3,53,40,20);
go
Create TABLE [dbo].[DEPARTMENTS]
(
 [DEPARTMENTS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [DEPARTMENTS_NAME][nvarchar](50) NOT NULL,
 [REGION_INFO][nvarchar](100) NULL
)
go
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'���������', N'����������� �������');
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'���������', N'����������� �������');
insert into [dbo].[DEPARTMENTS](DEPARTMENTS_NAME,REGION_INFO) values (N'�����', N'����������� �������');

go
Create TABLE [dbo].[POSITIONS]
(
 [POSITIONS_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [POSITIONS_NAME][nvarchar](50) NOT NULL,
)
go
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'��������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'��������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'������� ���������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'������');
insert into [dbo].[POSITIONS](POSITIONS_NAME) values (N'����������');

go
Create TABLE [dbo].[EMPLOYEES]
(
 [EMPLOYEES_ID] [int]IDENTITY(1,1) primary key NOT NULL,
 [EMPLOYEES_NAME][nvarchar](50) NOT NULL,
 [PHONE][nvarchar](15) NOT NULL,
 [MAIL][nvarchar](50) NOT NULL,
 [SALARY][int] NOT NULL,
 [DEPARTMENTS_ID][int] NOT NULL,
 [POSITIONS_ID][int] NOT NULL
)
go
ALTER TABLE [dbo].[EMPLOYEES] ADD CONSTRAINT FK_EMPLOYEES_DEPARTMENTS_ID
FOREIGN KEY (DEPARTMENTS_ID) REFERENCES [dbo].[DEPARTMENTS](DEPARTMENTS_ID);
ALTER TABLE [dbo].[EMPLOYEES] ADD CONSTRAINT FK_EMPLOYEES_POSITIONS_ID
FOREIGN KEY (POSITIONS_ID) REFERENCES [dbo].[POSITIONS](POSITIONS_ID);
go
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'���� ������',N'+387539475',N'gsjgsgks@kjghdgh.hf',8888,1,1);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'���� ������',N'+38675639475',N'674ks@kjghdgh.hf',4688,2,1);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'����� �������',N'+386766435',N'iejedsgks@kjghdgh.hf',1000,3,4);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'���� ������',N'+587539475',N'gsjgsgks@kjghdgh.���',88,2,2);
insert into [dbo].[EMPLOYEES](EMPLOYEES_NAME,PHONE,MAIL,SALARY,DEPARTMENTS_ID,POSITIONS_ID) values (N'���� ����',N'+7479475',N'lfjfgssgks@kjghdgh.hf',10000,3,3);
