use [db28pr6];
go
create table [dbo].[EmployeesToDepartments]
(
id int not null identity(1,1),
id_Employees int not null,
id_Departments int not null
);
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT PK_EmployeesToDepartments PRIMARY KEY (ID) ;
ALTER TABLE [dbo].[EmployeesToDepartments] ADD CONSTRAINT FK_EmployeesToDepartments_id_Employyees PRIMARY KEY (ID);
ALTER TABLE [dbo].[USRS] ADD CONSTRAINT FK_USRS FOREIGN KEY (id_deps) REFERENCES [dbo].DEPS(ID);
