use [db28pr6h];
create procedure calc(
@a INT,--input parametr 1
@b INT, --input parametr 2
@c int,--input parametr 3
@yn nvarchar(5) out,
@x1 float out,
@x2 float out
)
as
	begin 
		declare @D int
		set  @D=POWER(@b,2)-4*(@a*@c);
		if(@D>0)
			begin
				set @x1=-@b+SQRT(@D)/2*@a;
				set @x2=-@b-SQRT(@D)/2*@a;
				set @yn='yes';
			end
		else
			begin
				set @x1=0;
				set @x2=0;
				set @yn='no';
			end

	end

use [Northwind]
go
declare @my_sum as nvarchar(200)
execute dbo.PROC_FOR_TEST 1, 2, @my_sum output
select @my_sum as MYSUM




use db28pr6h;
Declare @res nvarchar(5)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
use [db28pr6h];
Execute [dbo].calc 1 ,0,-9, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure

use db28pr6h;
Declare @res nvarchar(5)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
use [db28pr6h];
Execute [dbo].calc 1 ,15,100, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure
