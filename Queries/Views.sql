use AdventureWorks2008;

/*������������� - ��� ����������� ������ (+�������). �� �� ������.
������������������� ������������� - ��� ����������� ������ - ��������� �������
*/

CREATE VIEW HumanResources.EmployeeHireDate  
AS  
SELECT p.FirstName, p.LastName, e.HireDate  
FROM HumanResources.Employee AS e JOIN Person.Person AS  p  
ON e.BusinessEntityID = p.BusinessEntityID ;   
GO  
-- Query the view  
SELECT FirstName, LastName, HireDate  
FROM HumanResources.EmployeeHireDate  
ORDER BY LastName;  


ALTER VIEW HumanResources.EmployeeHireDate  
AS  
SELECT p.FirstName, p.LastName, e.HireDate  
FROM HumanResources.Employee AS e JOIN Person.Person AS  p  
ON e.BusinessEntityID = p.BusinessEntityID ;  



/*OBJECT_ID  Returns the database object identification number of a schema-scoped object.
Syntax:
		OBJECT_ID ( '[ database_name . [ schema_name ] . | schema_name . ]   
		  object_name' [ ,'object_type' ] ) 
  
   */
USE AdventureWorks2008 ;  
GO  
IF OBJECT_ID ('hiredate_view', 'V') IS NOT NULL  
DROP VIEW hiredate_view ;  
GO  
CREATE VIEW hiredate_view  
AS   
SELECT p.FirstName, p.LastName, e.BusinessEntityID, e.HireDate  
FROM HumanResources.Employee e   
JOIN Person.Person AS p ON e.BusinessEntityID = p.BusinessEntityID ;  
GO  
select * from hiredate_view ;

--with encripting
USE AdventureWorks2008 ;   
GO  
IF OBJECT_ID ('Purchasing.PurchaseOrderReject', 'V') IS NOT NULL  
    DROP VIEW Purchasing.PurchaseOrderReject ;  
GO  
CREATE VIEW Purchasing.PurchaseOrderReject  
WITH ENCRYPTION  
AS  
SELECT PurchaseOrderID, ReceivedQty, RejectedQty,   
    RejectedQty / ReceivedQty AS RejectRatio, DueDate  
FROM Purchasing.PurchaseOrderDetail  
WHERE RejectedQty / ReceivedQty > 0  
AND DueDate > CONVERT(DATETIME,'20010630',101) ;  
GO 
select * from Purchasing.PurchaseOrderReject 


USE AdventureWorks2008;
GO
IF OBJECT_ID ( 'HumanResources.uspGetAllEmployees', 'P' ) IS NOT NULL 
    DROP PROCEDURE HumanResources.uspGetAllEmployees;
GO
CREATE PROCEDURE HumanResources.uspGetAllEmployees
AS
    SET NOCOUNT ON;
    SELECT LastName, FirstName, Department
    FROM HumanResources.vEmployeeDepartmentHistory;
GO

--how to use stored procedure
USE AdventureWorks2008;
GO
EXECUTE HumanResources.uspGetAllEmployees;
GO
-- Or
EXEC HumanResources.uspGetAllEmployees;
GO

HumanResources.uspGetAllEmployees;

USE AdventureWorks2008;
GO
select * from Purchasing.PurchaseOrderReject 


USE AdventureWorks2008;
GO
IF OBJECT_ID ( 'HumanResources.uspGetEmployees', 'P' ) IS NOT NULL 
    DROP PROCEDURE HumanResources.uspGetEmployees;
GO
CREATE PROCEDURE HumanResources.uspGetEmployees 
    @LastName nvarchar(50), 
    @FirstName nvarchar(50) 
AS 

    SET NOCOUNT ON;
    SELECT FirstName, LastName,Department
    FROM HumanResources.vEmployeeDepartmentHistory
    WHERE FirstName = @FirstName AND LastName = @LastName;
GO



USE AdventureWorks2008;
GO
IF OBJECT_ID ( 'HumanResources.uspGetEmployees2', 'P' ) IS NOT NULL 
    DROP PROCEDURE HumanResources.uspGetEmployees2;
GO
CREATE PROCEDURE HumanResources.uspGetEmployees2 
    @LastName nvarchar(50) = N'D%', 
    @FirstName nvarchar(50) = N'%'
AS 
    SET NOCOUNT ON;
    SELECT FirstName, LastName, Department
    FROM HumanResources.vEmployeeDepartmentHistory
    WHERE FirstName LIKE @FirstName AND LastName LIKE @LastName;
GO

USE AdventureWorks2008;
GO
EXECUTE HumanResources.uspGetEmployees2;
-- Or
EXECUTE HumanResources.uspGetEmployees2 N'Wi%';
-- Or
EXECUTE HumanResources.uspGetEmployees2 @FirstName = N'%';
-- Or
EXECUTE HumanResources.uspGetEmployees2 N'[CK]ars[OE]n';
-- Or
EXECUTE HumanResources.uspGetEmployees2 N'Hesse', N'Stefen';
-- Or
EXECUTE HumanResources.uspGetEmployees2 N'H%', N'S%';

USE AdventureWorks2008;
GO
IF OBJECT_ID ( 'HumanResources.uspEncryptThis', 'P' ) IS NOT NULL 
    DROP PROCEDURE HumanResources.uspEncryptThis;
GO
CREATE PROCEDURE HumanResources.uspEncryptThis
WITH ENCRYPTION
AS
    SET NOCOUNT ON;
    SELECT BusinessEntityID, JobTitle, NationalIDNumber, VacationHours, SickLeaveHours 
    FROM HumanResources.Employee;
GO

--��������� ���������sp_helptext:
EXEC sp_helptext 'HumanResources.uspEncryptThis';
EXEC sp_helptext 'HumanResources.uspGetEmployees2';

--�������� �������� ������ � ������������� ��������sys.sql_modules:
USE AdventureWorks2008;
GO
SELECT definition FROM sys.sql_modules
WHERE object_id = OBJECT_ID('HumanResources.uspEncryptThis');

use [Northwind]
go
create procedure PROC_FOR_TEST
(
@p1 int,--������ �������� 1 (������ in-��������� � C#)
@p2 int,--������� ������� 2
@out_sum varchar(200) out --�������� �������
)
as
begin
select @out_sum = @p1 + @p2;
end



use [db28pr6h];
create procedure calc(
@a INT,--input parametr 1
@b INT, --input parametr 2
@c int,--input parametr 3
@yn nvarchar(5) out,
@x1 float out,
@x2 float out
)
as
	begin 
		declare @D int
		set  @D=POWER(@b,2)-4*(@a*@c);
		if(@D>0)
			begin
				set @x1=-@b+SQRT(@D)/2*@a;
				set @x2=-@b-SQRT(@D)/2*@a;
				set @yn='yes';
			end
		else
			begin
				set @x1=0;
				set @x2=0;
				set @yn='no';
			end

	end

use [Northwind]
go
declare @my_sum as nvarchar(200)
execute dbo.PROC_FOR_TEST 1, 2, @my_sum output
select @my_sum as MYSUM




use db28pr6h;
Declare @res nvarchar(5)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
use [db28pr6h];
Execute [dbo].calc 1 ,0,-9, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure

use db28pr6h;
Declare @res nvarchar(5)   -- Declaring the variable to collect the discriminant
Declare @x1 float    -- Declaring the variable to collect the x1
Declare @x2 float    -- Declaring the variable to collect the x2
set @res = 'NOT def';
use [db28pr6h];
Execute [dbo].calc 1 ,15,100, @res output, @x1 output, @x2 output
select @res as d,@x1 as x1,@x2 as x2 -- "Select" Statement is used to show the output from Procedure
