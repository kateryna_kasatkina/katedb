--select top 10
/*
select 
[dbo].[Products].ProductName,
[dbo].[Products].UnitPrice,
[dbo].[Products].UnitsInStock,
[dbo].[Products].UnitsOnOrder
from
[dbo].[Products]
order by [dbo].[Products].UnitPrice asc
,[dbo].[Products].ProductName desc --desc
*/
select top 1
[dbo].[Products].ProductName,
[dbo].[Products].UnitPrice,
[dbo].[Products].UnitsInStock,
[dbo].[Products].UnitsOnOrder
from
[dbo].[Products]
order by [dbo].[Products].UnitPrice desc