--������� CAST � CONVERT (Transact-SQL)
--����������� ��������� ������ ���� ������ � ������
--���������
--Syntax for CAST:
--CAST ( expression AS data_type [ ( length ) ] )
--Syntax for CONVERT:
--CONVERT ( data_type [ ( length ) ] , expression [ , style ] )
--���������
--expression
--����� �������������������.
--data_type
--������� ��� ������.�� ����� ����� ������ ����������xml,�bigint��sql_variant.����������� ����� ������ �����������.
--length
--����������� ������������� ����� �����, ������������ ����� �������� ���� ������.��������� �� ��������� ����� 30.
--style
--������������� ���������, ������������, ��� ������� CONVERT ����������� ��������expression.����� ����� ����� �������� NULL, ������������ NULL.��������� ������������ ����������data_type.��������������� �������� ��. � ������� "�������".
--���� ������������ ��������
--���������� ��������expression, ��������������� � ���data_type.
use AdventureWorks2008;
--������ �������������� �����
declare @A varchar(2)
declare @B varchar(2)
declare @C varchar(2)
declare @D int
set @A = 25;
set @B = 15;
set @C = 35;
select cast(@A as int) + cast(@B as int)+ cast(@C as int) as Result ;
set @D = (select cast(@A as int) + cast(@B as int)+ cast(@C as int) as Result );
select @D as RESULT_IN_D

--������ 1
--� ������ ������� ���������� ����� ���������, � ������� ������ ����� ���� � 3, ��� ����� �� �������� ListPrice ����������������� � int.

SELECT SUBSTRING(Name, 1, 30) AS ProductName, ListPrice  
FROM Production.Product  
WHERE CAST(ListPrice AS int) LIKE '3%';

SELECT SUBSTRING(Name, 1, 30) AS ProductName, ListPrice  
FROM Production.Product  
WHERE CONVERT(int, ListPrice) LIKE '3%'; 

--������ 2 ������������� ������� CAST � ������������ LIKE
--� ��������� ������� ������� SalesYTD � ����� ������ money ������������� � ��� int, � ����� � ��� char(20) � ���, ����� ��� ����� ���� ������������ � ����������� LIKE

SELECT p.FirstName, p.LastName, s.SalesYTD, s.BusinessEntityID
FROM Person.Person AS p 
JOIN Sales.SalesPerson AS s 
    ON p.BusinessEntityID = s.BusinessEntityID
WHERE CAST(CAST(s.SalesYTD AS int) AS char(20)) LIKE '2%';


--������ 6 ������������� ������� CAST � CONVERT � ������� ���� datetime
--��������� ������ ���������� ������� ���� � �����, ���������� ������� CAST ��� ��������� ������� ���� � ������� � ���������� ��� ������ � ����� ���������� CONVERT ��� ����������� ���� � ������� � ������� ISO 8901.

SELECT 
   GETDATE() AS UnconvertedDateTime,
   CAST(GETDATE() AS nvarchar(30)) AS UsingCast,
   CONVERT(nvarchar(30), GETDATE(), 126) AS UsingConvertTo_ISO8601  





   --������ 1 ������������� ��������� ���������� IF-ELSE
DECLARE @Number int;
SET @Number = 50;
IF @Number > 100
���PRINT 'The number is large.';
ELSE 
���BEGIN
������IF @Number < 10
������PRINT 'The number is small.';
���ELSE
������PRINT 'The number is medium.';
���END ;

--������ 2
--� ��������� ������� ������ ����������� ��� ����� ����������� ���������, � ����� ����������� ������� ������������ ����� ����������, ���������� �� ���������� ����������� ���������. ������ ���� ���������� ���������� �BEGIN�� ������������END.
DECLARE @AvgWeight decimal(8,2), @BikeCount int
IF 
(SELECT COUNT(*) FROM Production.Product WHERE Name LIKE 'Touring-3000%' ) > 5
BEGIN
���SET @BikeCount = 
��������(SELECT COUNT(*) 
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%');
���SET @AvgWeight = 
��������(SELECT AVG(Weight) 
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%');
���PRINT 'There are ' + CAST(@BikeCount AS varchar(3))�+ ' Touring-3000 bikes.'
���PRINT 'The average weight of the top 5 Touring-3000 bikes is ' + CAST(@AvgWeight AS varchar(8)) + '.';
END
ELSE 
BEGIN
SET @AvgWeight = 
��������(SELECT AVG(Weight)
���������FROM Production.Product 
���������WHERE Name LIKE 'Touring-3000%' );
���PRINT 'Average weight of the Touring-3000 bikes is ' + CAST(@AvgWeight AS varchar(8)) + '.' ;
END ;


--������ 3 ������������� �������� ���� BREAK � CONTINUE ������ ��������� ����������� IF...ELSE � WHILE
--���� ������� ���� ��������� �� ������ ������ ���$300, ����WHILE���������� ����, � ����� �������� ������������. � ��� ������, ���� ������������ ���� ������ ��� �����$500, ����WHILE������������ � ����� ��������� ����. ���� ���� ���������� ��������� ���� �� ��� ���, ���� ������������ ���� �� ����� ������ ���$500, ����� ���������� �����WHILE�������������, � ��� ��������� ��������������� ���������.
WHILE (SELECT AVG(ListPrice) FROM Production.Product) < $300
BEGIN
   UPDATE Production.Product
      SET ListPrice = ListPrice * 2
   SELECT MAX(ListPrice) FROM Production.Product
   IF (SELECT MAX(ListPrice) FROM Production.Product) > $500
      BREAK
   ELSE
      CONTINUE
END
PRINT 'Too much for the market to bear';


   